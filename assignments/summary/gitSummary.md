## What is Git?

Git is a distributed version control system, which is software that allows developers to maintain a complete history of every file in a project. However, you don't need to be a developer to use Git with your website. For example, you can use Git to maintain a history of all of your website files: every time a file is changed, a version of the change is recorded in the Git software. If necessary, you can always go back to any version and restore it.

Version control system is explained by below picture:

![Vcs](https://git-scm.com/book/en/v2/images/centralized.png)
The core of Git was originally written in the programming language C, but Git has also been re-implemented in other languages, e.g., Java, Ruby and Python.

However, Git is a distributed version control system. It is specifically explained by below:

![Dvcs](https://miro.medium.com/max/3396/1*GgaGcwh5L246YcU5NVDA5A.png)
## Git Repositories

A Git repository contains the history of a collection of files starting from a certain directory. The process of copying an existing Git repository via the Git tooling is called cloning. After cloning a repository the user has the complete repository with its history on his local machine. Of course, Git also supports the creation of new repositories.

![Repo](https://media.geeksforgeeks.org/wp-content/uploads/20191122182103/staging_process.jpg)
## Working Tree

A local repository provides at least one collection of files which originate from a certain version of the repository. This collection of files is called the working tree. The user can change the files in the working tree by modifying existing files and by creating and removing files.

A file in the working tree of a Git repository can have different states. These states are the following:

- untracked: the file is not tracked by the Git repository. This means that the file never staged nor committed.
- tracked: committed and not staged
- staged: staged to be included in the next commit
- dirty / modified: the file has changed but the change is not staged

![Working Tree](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRr0eGHeH3mIAudOp7qUJ-cSN2XPssvfc9fWQ&usqp=CAU)

## Git Workflow

The following actions are the steps of Git workflow:

1. Clone the repository
2. git fetch and git rebase
3. Create a new branch
4. Add commits
5. git fetch and git rebase (on the master branch)
6. Push the commits
7. Create a Pull Request
8. Discuss, review and merge pull request

It can be explained through a flowchart:

![Git Workflow](https://miro.medium.com/max/577/1*AAU1VCV8LMHvVPBYxMBsxg.png)
## Various Terminologies

### Commit

When you commit to a repository, it’s like you’re taking snapshot of the files as they exist at that moment. The commit will only exist on your local machine until it is pushed to a remote repository.

### Push 

Pushing is essentially syncing your commits to the repository.

### Branch

You can think of your git repo as a tree. The trunk of the tree, the main software, is called the "Master Branch". The branches of that tree are, well, called branches. These are separate instances of the code that is different from the main codebase.

### Merge

When a branch is free of bugs (as far as you can tell, at least), and ready to become part of the primary codebase, it can be merged into the master branch. Merging is just what it sounds like: integrating two branches together.

### Clone

Cloning a repo is pretty much exactly what it sounds like. It takes the entire online repository and "makes an exact copy" of it on your local machine.

### Fork

Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.


## Git Commands

- `git config` - This command sets the author name and email address respectively to be used with your commits.

- `git init` - This command is used to start a new repository.

- `git clone` - This command is used to obtain a repository from an existing URL.

- `git add` - This command adds a file to the staging area.
 
- `git commit` - This command records or snapshots the file permanently in the version history.
 
- `git diff` - This command shows the file differences which are not yet staged.

- `git reset` - This command unstages the file, but it preserves the file contents.
 
- `git status` - This command lists all the files that have to be committed.
 
- `git rm` - This command deletes the file from your working directory and stages the deletion.
 
- `git log` - This command is used to list the version history for the current branch.

- `git show` - This command shows the metadata and content changes of the specified commit.

- `git tag` - This command is used to give tags to the specified commit.
 
- `git branch` - This command lists all the local branches in the current repository.
 
- `git checkout` - This command is used to switch from one branch to another.
 
- `git push` - This command sends the committed changes of master branch to your remote repository.
 
- `git pull` - This command fetches and merges changes on the remote server to your working directory.
 
- `git stash` - This command temporarily stores all the modified tracked files.

Examples:

```bash 
$ git config –global user.name “[name]”
$ git config –global user.email “[email address]”
```

```bash 
$ git clone [url]
```



